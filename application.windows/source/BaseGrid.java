import processing.core.PApplet;
import processing.core.PVector;

public class BaseGrid {
	
	PApplet parent;
	int baseWidth = 32;
	int baseHeight = 20;
	int rows = 8;
	int cols = 28;
	int xPos = 50;
	int yPos = 200;
	int randomRadius = 20;
	int randomRadiusSq = randomRadius * randomRadius;
	
	boolean draw = false;
	
	LineRef[] logoLines = new LineRef[10];
	
	public BaseGrid(PApplet parent){
		this.parent = parent;
		assignGridRefs();
	}
	
	private void assignGridRefs(){
		logoLines[0] = new LineRef(0, 8, 4, 0);
		logoLines[1] = new LineRef(3, 0, 5, 8);
		logoLines[2] = new LineRef(7, 8, 8, 0);
		logoLines[3] = new LineRef(7, 8, 11, 8);
		logoLines[4] = new LineRef(13, 8, 14, 0);
		logoLines[5] = new LineRef(16, 0, 18, 8);
		logoLines[6] = new LineRef(17, 8, 21, 0);
		logoLines[7] = new LineRef(22, 8, 27, 8);
		logoLines[8] = new LineRef(22.5f, 5, 27.5f, 3);
		logoLines[9] = new LineRef(23, 0, 28, 0);
	}
	
	private void drawRandomRadius(){
		parent.noFill();
		parent.stroke(255,0,255);
		parent.strokeWeight(1);
		
		for (int i = 0; i < logoLines.length; i++) {
			parent.ellipse(xPos + logoLines[i].ax * baseWidth, yPos + logoLines[i].ay * baseHeight, randomRadius*2, randomRadius*2);
			parent.ellipse(xPos + logoLines[i].bx * baseWidth, yPos + logoLines[i].by * baseHeight, randomRadius*2, randomRadius*2);
		}
	}
	
	public PVector[] getLineVectors(int i){
		PVector[] p = new PVector[2];
		p[0] = randomisePoint(new PVector(xPos + logoLines[i].ax * baseWidth, yPos + logoLines[i].ay * baseHeight));
		p[1] = randomisePoint(new PVector(xPos + logoLines[i].bx * baseWidth, yPos + logoLines[i].by * baseHeight));
		return p;
	}
	
	// randomises point within radius around current point
	private PVector randomisePoint(PVector p){
		float x=parent.random(-randomRadius,randomRadius);
		float y=parent.random(-1,1)*parent.sqrt(randomRadiusSq-x*x);
		return new PVector(p.x + x, p.y + y);
	}
	
	public void draw(){
		if(!draw)return;
		
		parent.stroke(200);
		parent.strokeWeight(1);
		
		for (int i = 0; i < rows+1; i++) {
			parent.line(xPos, yPos + baseHeight * i, xPos + baseWidth * cols, yPos + baseHeight * i);
		}
		for (int i = 0; i < cols+1; i++) {
			parent.line(xPos + baseWidth * i, yPos, xPos + baseWidth * i, yPos + baseHeight * rows);
		}

		drawRandomRadius();
	}

}
