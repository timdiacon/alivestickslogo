import java.util.ArrayList;

import controlP5.ControlEvent;
import processing.core.PApplet;

public class AliveSticks extends PApplet {

	Stick s;
	BaseGrid grid;
	
	ArrayList<Stick> sticks;
	int stickTransitionsComplete;
	Gui gui;
	
	float emissionRate = 1;
	boolean exporting = false;
	
	// auto pilot stuff
	boolean autoPilotEnabled = true;
	float emissionSinCount = 0;
	float emissionSinStep = 0.2f;
	
	float transitionSinCount = 0;
	float transitionSinStep = 0.5f;
	
	float delaySinCount = 0;
	float delaySinStep = 1;
	
	float traceStepSinCount = 0;
	float traceStepSinStep = 0.3f;
	
	float traceLifeSinCount = 0;
	float traceLifeSinStep = 0.1f;
	
	
	public void setup(){
		size(1000,400);
		smooth();
		grid = new BaseGrid(this);
		
		gui = new Gui(this, grid);
		gui.init();

		sticks = new ArrayList<Stick>();

		gui.sticks = sticks;
	}
	
	// required for standalone
	public static void main(String args[]) {
		PApplet.main(new String[] { "--present", "AliveSticks" });
	}
	
	private void moveStickForward(Stick s){
		if(s.direction == 1){
			if(s.position + 1 < grid.logoLines.length){
				s.move(s.position + 1, grid.getLineVectors(s.position + 1));
			} else {
				s.die();
			}
		} else {
			if(s.position - 1 >= 0){
				s.move(s.position - 1, grid.getLineVectors(s.position - 1));
			} else {
				s.die();
			}
		}
	}
	
	private void addNewStick(){
		int randPos = (int) random(grid.logoLines.length);
		Stick ns = new Stick(this, randPos, grid.getLineVectors(randPos));
		sticks.add(ns);
		
		// apply current settings
		ns.minTransitionDur = gui.cp5.getController("minTransitionDur").getValue();
		ns.maxTransitionDur = gui.cp5.getController("maxTransitionDur").getValue();
		ns.maxDelay = gui.cp5.getController("maxDelay").getValue();
		ns.updateTraceLife(gui.cp5.getController("traceLife").getValue());
		ns.traceStep = (int) gui.cp5.getController("traceStep").getValue();
		ns.easeType = Ease.EaseTypes.values()[(int) gui.cp5.getGroup("easeType").getValue()];
		
		// set it off...
		moveStickForward(ns);
	}
	
	public void draw(){
		if(exporting){
			beginRecord(PDF, "exported-frames/frame-####.pdf"); 
		}
		
		background(255,255,255);
		grid.draw();
		
		for (int i = 0; i < sticks.size(); i++) {
			sticks.get(i).update();
		}

		// Create new sticks based on emission rate
		if(emissionRate > 0 && frameCount%(Math.round(60/emissionRate)) == 0){
			addNewStick();
		}
		
		if (exporting) {
			endRecord();
			exporting = false;
		}
		
		if(autoPilotEnabled){
			// emission rate
			emissionSinCount = (emissionSinCount + emissionSinStep < 360) ? emissionSinCount + emissionSinStep : 0;
			gui.cp5.getController("emissionRate").setValue(map(sin(radians(emissionSinCount)), -1, 1, 0, 2));
			// max transition
			transitionSinCount = (transitionSinCount + transitionSinStep < 360) ? transitionSinCount + transitionSinStep : 0;
			gui.cp5.getController("maxTransitionDur").setValue(map(sin(radians(transitionSinCount)), -1, 1, 2, 5));
			// max transition
			delaySinCount = (delaySinCount + delaySinStep < 360) ? delaySinCount + delaySinStep : 0;
			gui.cp5.getController("maxDelay").setValue(map(sin(radians(delaySinCount)), -1, 1, 0, 5));
			// trace step
			traceStepSinCount = (traceStepSinCount + traceStepSinStep < 360) ? traceStepSinCount + traceStepSinStep : 0;
			gui.cp5.getController("traceStep").setValue(map(sin(radians(traceStepSinCount)), -1, 1, 2, 20));
			// trace life
			traceLifeSinCount = (traceLifeSinCount + traceLifeSinStep < 360) ? traceLifeSinCount + traceLifeSinStep : 0;
			gui.cp5.getController("traceLife").setValue(map(sin(radians(traceLifeSinCount)), -1, 1, 0.2f, 5));
		}

	}
	
	// EVENT HANDLERS
	
	// controlP5
	public void controlEvent(ControlEvent e){
		gui.handleEvent(e);
	}
	
	// called directly from Stick
	public void tweenComplete(Stick s){
		moveStickForward(s);		
	}
	
	// called directly from Stick
	public void stickDied(Stick s){
		sticks.remove(s);
		s = null;
	}
	
	// keyboard events
	public void keyPressed() {
		//println("Key:" + key);
		switch (key) {
		case '.':
			for (int i = 0; i < sticks.size(); i++) {
				moveStickForward(sticks.get(i));
			}
			break;
		case 'e':
			exporting = true;
			break;
		case 'a':
			autoPilotEnabled = (autoPilotEnabled) ? false : true;
			break;
		default:
			break;
		}
	}
	
}
