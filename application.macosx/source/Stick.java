import java.util.ArrayList;
import processing.core.PVector;

public class Stick {

	AliveSticks parent;
	PVector a;
	PVector b;
	PVector ta;
	PVector tb;
	PVector pa;
	PVector pb;
	ArrayList<Trace> traces;
	
	int position;
	int time;
	int duration;
	int delay;
	int direction;
	
	float pickWidth = 0;
	float pickCap = 0;
	
	float targetPickWidth = 5;
	float targetPickCap = 10;
	
	float minTransitionDur;
	float maxTransitionDur;
	float maxDelay;
	int traceStep;
	
	float traceLife;
	boolean tweening = false;

	boolean dying = false;
	int deathDuration = 100;
	int deathTime;
	
	boolean beingBorn = true;
	int birthDuration = 100;
	int birthTime = 0;
	
	Ease.EaseTypes easeType;
	
	public Stick(AliveSticks parent, int position, PVector[] p){
		this.parent = parent;
		this.position = position;
		direction = (int) Math.round(Math.random());
		a = p[0];
		b = p[1];
		ta = a;
		tb = b;
		traces = new ArrayList<Trace>();
	}
	
	public void updateGrid(PVector[] p){
		a = p[0];
		b = p[1];
		ta = a;
		tb = b;
	}
	
	public void updateTraceLife(float life){
		traceLife = life;
		for (int i = 0; i < traces.size(); i++) {
			Trace t = (Trace) traces.get(i);
			t.updateLife(traceLife);
		}
	}

	public void move(int pos, PVector[] p){
		pa = new PVector(a.x, a.y);
		pb = new PVector(b.x, b.y);
		
		tweening = true;
		time = 0;
		duration = (int) parent.random(minTransitionDur, maxTransitionDur) * 60;
		delay = (int) parent.random(maxDelay) * 60;
		
		position = pos;
		
		this.ta = new PVector(p[0].x, p[0].y);
		this.tb = new PVector(p[1].x, p[1].y);
	}
	
	public void die(){
		deathTime = 0;
		dying = true;
	}
	
	public void update(){
		parent.noFill();
		
		// check for a delay
		if(delay > 0){
			delay--;
		// if delay gone start moving
		} else if(tweening) {
			// if we finished tweening...
			if(time == duration){
				tweening = false;
				parent.tweenComplete(this);	
			// do some easing shit!
			} else {				
				a.x = Ease.ease(time, pa.x, ta.x - pa.x, duration, easeType);
				a.y = Ease.ease(time, pa.y, ta.y - pa.y, duration, easeType);
				b.x = Ease.ease(time, pb.x, tb.x - pb.x, duration, easeType);
				b.y = Ease.ease(time, pb.y, tb.y - pb.y, duration, easeType);
				time++;
				// add trace at step interval
				if(time%traceStep==0){
					traces.add(new Trace(parent, this, new PVector(a.x, a.y),  new PVector(b.x, b.y), traceLife));
				}
			}
		}
		
		// update the traces
		for (int i = 0; i < traces.size(); i++) {
			Trace t = (Trace) traces.get(i);
			t.update();
		}
		
		drawMainLine();
	}
	
	public void traceComplete(Trace t){
		traces.remove(t);
		t = null;
	}
	
	private void drawMainLine(){
		
		if(beingBorn){
			pickWidth = Ease.ease(birthTime, pickWidth, targetPickWidth - pickWidth, birthDuration, Ease.EaseTypes.easeInQuad);
			pickCap = Ease.ease(birthTime, pickCap, targetPickCap - pickCap, birthDuration, Ease.EaseTypes.easeInQuad);
			birthTime++;
			if(birthTime == birthDuration){
				beingBorn = false;
			}
		}

		if(dying){
			pickWidth = Ease.ease(deathTime, pickWidth, 0 - pickWidth, deathDuration, Ease.EaseTypes.easeInQuad);
			pickCap = Ease.ease(deathTime, pickCap, 0 - pickCap, deathDuration, Ease.EaseTypes.easeInQuad);
			deathTime++;
			if(deathTime == deathDuration){
				dying = false;
				// update parent that unfortunately I died
				parent.stickDied(this);
			}
		}
		
		parent.noStroke();
		parent.fill(0);
		// calculate angle of line
    	float angle = parent.degrees(parent.atan2(b.y-a.y, b.x-a.x));
    	
    	float[] f;
    	parent.beginShape();
    	f = translatePoint(a.x, a.y, pickWidth/2, parent.radians(angle - 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(b.x, b.y, pickWidth/2, parent.radians(angle - 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(b.x, b.y, pickCap, parent.radians(angle));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(b.x, b.y, pickWidth/2, parent.radians(angle + 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(a.x, a.y, pickWidth/2, parent.radians(angle + 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(a.x, a.y, -pickCap, parent.radians(angle));
    	parent.vertex(f[0], f[1]);
    	
    	parent.endShape(parent.CLOSE);

	}
	
	private float[] translatePoint(float x, float y, float d, float a){
		float[] cords = new float[2];
		cords[0] = x + d * parent.cos(a);
		cords[1] = y + d * parent.sin(a);
		return cords;
	}
}
