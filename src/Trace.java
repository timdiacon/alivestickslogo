import processing.core.PApplet;
import processing.core.PVector;

public class Trace {
	
	PApplet parent;
	Stick dad;
	PVector a;
	PVector b;
	
	float aStartX;
	float aStartY;
	
	float pickWidth = 2;
	float pickCap = 10;
	
	int life;
	int age = 0;
	int ageSpeed = 1;
	float driftSpeed = 0.1f;
	float driftAngle;
	
	
	public Trace(PApplet parent, Stick dad, PVector a, PVector b, float life){
		this.parent = parent;
		this.dad = dad;
		this.a = a;
		this.b = b;
		this.life = (int) life * 60;
		
		aStartX = a.x;
		aStartY = a.y;
		driftAngle = parent.degrees(parent.atan2(b.y-a.y, b.x-a.x));
	}
	
	public void update(){
		if(age == life){
			dad.traceComplete(this);
			return;
		}

		parent.noStroke();
		parent.fill(0);
		//drift();
		draw();
		sink();
		age++;
		
	}
	
	public void updateLife(float life){
		this.life = (int) life * 60;
	}
	
	// shortens the line
	private void sink(){
		a.x = Ease.ease(age, a.x, b.x - a.x, life, Ease.EaseTypes.easeInOutQuad);
		a.y = Ease.ease(age, a.y, b.y - a.y, life, Ease.EaseTypes.easeInOutQuad);
		pickWidth = Ease.ease(age, pickWidth, 0 - pickWidth, life, Ease.EaseTypes.easeInOutQuad);
		pickCap = Ease.ease(age, pickCap, 0 - pickCap, life, Ease.EaseTypes.easeInOutQuad);
	}
	
	private void drift(){
		float[] f;
		f = translatePoint(a.x, a.y, driftSpeed, parent.radians(driftAngle - 90));
		a.x = f[0];
		a.y = f[1];
		
		f = translatePoint(b.x, b.y, driftSpeed, parent.radians(driftAngle - 90));
		b.x = f[0];
		b.y = f[1];
	}
	
	private void draw(){
		// calculate angle of line
    	float angle = parent.degrees(parent.atan2(b.y-a.y, b.x-a.x));
    	
    	float[] f;
    	parent.beginShape();
    	f = translatePoint(a.x, a.y, pickWidth/2, parent.radians(angle - 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(b.x, b.y, pickWidth/2, parent.radians(angle - 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(b.x, b.y, pickCap, parent.radians(angle));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(b.x, b.y, pickWidth/2, parent.radians(angle + 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(a.x, a.y, pickWidth/2, parent.radians(angle + 90));
    	parent.vertex(f[0], f[1]);
    	
    	f = translatePoint(a.x, a.y, -pickCap, parent.radians(angle));
    	parent.vertex(f[0], f[1]);
    	
    	parent.endShape(parent.CLOSE);

	}
	
	public float[] translatePoint(float x, float y, float d, float a){
		float[] cords = new float[2];
		cords[0] = x + d * parent.cos(a);
		cords[1] = y + d * parent.sin(a);
		return cords;
	}
}
