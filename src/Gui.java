import java.util.ArrayList;

import controlP5.*;

public class Gui {
	
	AliveSticks parent;
	ControlP5 cp5;
	Boolean inited = false;

	BaseGrid baseGrid;
	ArrayList<Stick> sticks;
	
	Slider gridWidth;
	Slider gridHeight;
	
	public Gui(AliveSticks parent, BaseGrid baseGrid){
		this.parent = parent;
		this.baseGrid = baseGrid;
	}
	
	public void init(){
		
		cp5 = new ControlP5(parent)
			.setColorCaptionLabel(0);
		
		cp5.addSlider("gridWidth")
			.setId(0)
			.setPosition(10, 10)
			.setRange(10, 50)
			.setValue(baseGrid.baseWidth)
			.setCaptionLabel("Grid width");
		
		cp5.addSlider("gridHeight")
			.setId(1)
			.setPosition(10, 30)
			.setRange(10, 50)
			.setValue(baseGrid.baseHeight)
			.setCaptionLabel("Grid height");
		
		cp5.addSlider("randomRadius")
			.setId(10)
			.setPosition(10, 50)
			.setDecimalPrecision(1)
			.setRange(0, 50)
			.setValue(20)
			.setCaptionLabel("Random radius");
		
		cp5.addToggle("displayGrid")
			.setId(7)
			.setPosition(10, 70)
			.setValue(0)
			.setCaptionLabel("Display Grid")
			.getCaptionLabel().align(ControlP5.RIGHT_OUTSIDE, ControlP5.CENTER).setPaddingY(0).setPaddingX(5);
		
		cp5.addSlider("minTransitionDur")
			.setId(2)
			.setPosition(200, 10)
			.setDecimalPrecision(1)
			.setRange(0.5f, 5)
			.setValue(1)
			.setCaptionLabel("Min transition duration");
					
		cp5.addSlider("maxTransitionDur")
			.setId(3)
			.setPosition(200, 30)
			.setDecimalPrecision(1)
			.setRange(0.5f, 5)
			.setValue(2)
			.setCaptionLabel("Max transition duration");
		
		cp5.addSlider("maxDelay")
			.setId(4)
			.setPosition(200, 50)
			.setDecimalPrecision(1)
			.setRange(0, 5)
			.setValue(5)
			.setCaptionLabel("Max Delay");
		
		DropdownList ddEase = cp5.addDropdownList("easeType")
			.setId(9)
			.setPosition(200, 80)
			.setColorLabel(parent.color(255));
			
			for (int i = 0; i < Ease.EaseTypes.values().length; i++) {
				ddEase.addItem(Ease.EaseTypes.values()[i].toString(), i);				
			}
			
			ddEase.setValue(3);
			
		cp5.addSlider("traceStep")
			.setId(5)
			.setPosition(450, 10)
			.setRange(2, 20)
			.setDecimalPrecision(0)
			.setValue(10)
			.setCaptionLabel("Trace step");
		
		cp5.addSlider("traceLife")
			.setId(6)
			.setPosition(450, 30)
			.setRange(0.2f, 5)
			.setDecimalPrecision(1)
			.setValue(1)
			.setCaptionLabel("Trace life");
		
		cp5.addSlider("emissionRate")
			.setId(8)
			.setPosition(640, 10)
			.setRange(0, 2)
			.setDecimalPrecision(1)
			.setValue(1)
			.setCaptionLabel("Emission Rate");
		
		inited = true;
	}
	
	public void handleEvent(ControlEvent e){
		if(inited == false) return;
		
		switch (e.getId()) {
		case 0: // getWidth
			baseGrid.baseWidth = (int) e.getController().getValue();
			for (int i = 0; i < sticks.size(); i++) {
				sticks.get(i).updateGrid(baseGrid.getLineVectors(i));
			}
			break;
		case 1: // getHeight
			baseGrid.baseHeight = (int) e.getController().getValue();
			for (int i = 0; i < sticks.size(); i++) {
				sticks.get(i).updateGrid(baseGrid.getLineVectors(i));
			}
			break;
		case 7: // showGrid
			baseGrid.draw = (e.getController().getValue() == 1) ? true : false;
			break;
		case 8: // emissionRate
			parent.emissionRate = e.getController().getValue();
			break;
		case 9: // ease
			for (int i = 0; i < sticks.size(); i++) {
				sticks.get(i).easeType = Ease.EaseTypes.values()[(int) cp5.getGroup("easeType").getValue()];
			}
			break;
		case 10: // randomRadius
			baseGrid.randomRadius =(int) e.getController().getValue();
			baseGrid.randomRadiusSq = baseGrid.randomRadius * baseGrid.randomRadius;
			break;
		default:
			break;
		}
	}
}
