
public class Ease {
	
	// Simple port of Penner's easing equations.... Thanks Mr Penner!
	
	// t = current time
	// b = start value
	// c = change in value
	// d = duration
	
	private static float s = 1.70158f; // used in back equation
	
	public static enum EaseTypes{
		noease,
		easeInQuad,easeOutQuad,easeInOutQuad,
		easeInCubic,easeOutCubic,easeInOutCubic,
		easeInBack,easeOutBack,easeInOutBack,
		easeInCirc,easeOutCirc,easeInOutCirc,
		easeInExpo,easeOutExpo,easeInOutExpo,
		easeInSine,easeOutSine,easeInOutSine,
	}
	
	public static float ease(float t, float b, float c, float d, EaseTypes ease){
		float f = 0;
		switch (ease) {
		case noease:
			f = c*t/d + b;
			break;
		case easeInQuad:
			f = c*(t/=d)*t + b;
			break;
		case easeOutQuad:
			f = -c *(t/=d)*(t-2) + b;
			break;
		case easeInOutQuad:
			f = ((t/=d/2) < 1) ? c/2*t*t + b : -c/2 * ((--t)*(t-2) - 1) + b;				
			break;
		case easeInCubic:
			f = c*(t/=d)*t*t + b;
			break;
		case easeOutCubic:
			f = c*((t=t/d-1)*t*t + 1) + b;
			break;
		case easeInOutCubic:
			f = ((t/=d/2) < 1) ? c/2*t*t*t + b : c/2*((t-=2)*t*t + 2) + b;				
			break;
		case easeInBack:
			f = c*(t/=d)*t*((s+1)*t - s) + b;
			break;
		case easeOutBack:
			//f = c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
			break;
		case easeInOutBack:
			f = ((t/=d/2) < 1) ? c/2*(t*t*(((s*=(1.525))+1)*t - s)) : c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;			
			break;	
		case easeInCirc:
			f = (float) (-c * (Math.sqrt(1 - (t/=d)*t) - 1) + b);
			break;
		case easeOutCirc:
			f = (float) (c * Math.sqrt(1 - (t=t/d-1)*t) + b);
			break;
		case easeInOutCirc:
			f = ((t/=d/2) < 1) ? (float) (-c/2 * (Math.sqrt(1 - t*t) - 1) + b) : (float) (c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b);			
			break;
		case easeInExpo:
			f = (t==0) ? b : (float) (c * Math.pow(2, 10 * (t/d - 1)) + b);
			break;
		case easeOutExpo:
			f = (t==d) ? b+c : (float) (c * (-Math.pow(2, -10 * t/d) + 1) + b);
			break;
		case easeInOutExpo:
			if (t==0){
				f = b;
			} else if (t==d){
				f = b+c;
			} else if ((t/=d/2) < 1){
				f = (float) (c/2 * Math.pow(2, 10 * (t - 1)) + b);
			} else {
				f = (float) (c/2 * (-Math.pow(2, -10 * --t) + 2) + b);		
			}
			break;	
			
		case easeInSine:
			f = (float) (-c * Math.cos(t/d * (Math.PI/2)) + c + b);
			break;
		case easeOutSine:
			f = (float) (c * Math.sin(t/d * (Math.PI/2)) + b);
			break;
		case easeInOutSine:
			f = (float) (-c/2 * (Math.cos(Math.PI*t/d) - 1) + b);			
			break;
		default:
			break;
		}
		return f;
	}

}
